from setuptools import setup, find_packages
from os.path import join, dirname


setup(
    name='poet',
    version='3.0',
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    test_suite='tests',
    install_requires=[
        'trafaret==2.0.2',
        # OPTIONAL DEPENDENCY FOR CALCULATION RUSSIAN TAGS 'simple-phrase-trigger==1.1'
    ],
    entry_points={
        'console_scripts': [
            'import_words=poet.commands.import_words:main',
            'import_phrases=poet.commands.import_phrases:main',
            'change_phrase=poet.commands.change_phrase:main',
            'change_word=poet.commands.change_word:main',
            'migrate_phrase_base=poet.commands.migrate_phrase_base:main',
        ],
    }
)
