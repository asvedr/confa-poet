# confa-poet
The library designed for creating poems from set of phrases 

# How to
## 1. Install
```sh
python3 setup.py install
```
## 2. Prepare database of words
```sh
import_words dicts/english.js-eng-code.txt words.db
```
If you need more words add file in `dicts/` or add the words in prepared base using the same script `import_words <your_file> words.db`.
If `words.db` already exists then the base will be updated not rewrited. Check the `--help` option
## 3. Prepare phrase base
```sh
import_phrases <source.txt> words.db <language> phrases.db
```
This script will compile file with phrases `source.txt` to database of rhymed phrases which you can use later. If `phrases.db` already exists it will be updated not rewrited. Input file is phrases separated with '\n' symbol.
## 4. Generate rhyme
```python
from poet import Poet

config = {
    'yamb': {
        'sizes': [9, 8, 9, 8],
        'rhymes': [[0, 2], [1, 3]]
    }
}

poet = Poet.load('phrases.db', config)
verse = poet.gen_random_verse()
```
## 5. If some phrase detected with incorrect ending
Use script `change_ending` to edit ending of concrete phrase.
```sh
change_ending rogov.db 'Рубрика "знакомые лица"' -s 'ица'
```


# confa-poet
Автопоэт для сочинения стихов из готовых предложений

# Как использовать
## 1. Установть либу
```sh
python3 setup.py install
```
## 2. Подготовить базу ударений слов
```sh
import_words dicts/1.hagen.txt words.db
```
При необходимости добавьте свой файл со словами в `dicts/` или добавьте его позже в готовую базу тем же скриптом. `import_words <your_file> words.db`. Если `words.db` уже существует, то база будет обновлена, а не перезаписана. Скипт снабжен параметром `--help` с доп инфой
## 3. Подготовить базу фраз
```sh
import_phrases <source.txt> words.db <language> phrases.db
```
Этот скрипт на основе фраз из файла `source.txt` сделает базу рифмующихся строк, которую после можно будет использовать. Если `phrases.db` уже существует, то он будет обновлен, а не перезаписан. Для доп параметров смотрите вывод команды с ключем `--help`Входной файл это текстовый файл где каждая строка - фраза.
## 4. Генерация стихов
```python
from poet import Poet

config = {
    'yamb': {
        'sizes': [9, 8, 9, 8],
        'rhymes': [[0, 2], [1, 3]]
    }
}

poet = Poet.load('phrases.db', config)
verse = poet.gen_random_verse()
```
## 5. В одной из фраз неправильное ударение
Если после генерации баз выяснилось, что где-то неправильно поставилось ударение, поправить одну фразу можно скриптом `change_ending`. Help внутри скрипта.
```sh
change_ending rogov.db 'Рубрика "знакомые лица"' -s 'ица'
```
