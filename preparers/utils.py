import sqlite3


def make_flat(rows):
    for i in range(len(rows)):
        rows[i] = rows[i][0]


class ConfaBase(object):

    def __init__(self, path):
        self._base = sqlite3.connect(path)

    def fetchall(self, *args, **kwargs):
        flat = kwargs.get('flat')
        rows = self._base.execute(*args).fetchall()
        if not rows:
            return []
        if flat:
            make_flat(rows)
        return rows

    def fetchone(self, *args, **kwargs):
        flat = kwargs.get('flat')
        rows = self._base.execute(*args).fetchone()
        if not rows:
            return None
        if flat:
            return rows[0]
        return rows

    def get_user_id(self, user):
        return self.fetchone('SELECT id FROM user WHERE name = ?', [user], flat=True)

    def get_users(self):
        return self.fetchall('SELECT name FROM user', flat=True)

    def rename(self, username, new_username):
        self._base.execute('UPDATE user SET name = ? WHERE name = ?', [new_username, username])
        self._base.commit()

    def user_replies_to_text(self, user):
        user = self.get_user_id(user)
        return self.fetchall('''
            SELECT reply.data, msg.data
            FROM message as msg JOIN message as reply ON msg.id = reply.reply_to
            WHERE msg.is_fake = 0 and reply.user_id = ?
        ''', [user])

    def user_replies_to_external(self, user):
        user = self.get_user_id(user)
        return self.fetchall('''
            SELECT reply.data
            FROM message as msg JOIN message as reply ON msg.id = reply.reply_to
            WHERE msg.is_fake = 1 and reply.user_id = ?
        ''', [user], flat=True)

    def user_replies_to_user(self, user_a, user_b):
        user_a = self.get_user_id(user_a)
        user_b = self.get_user_id(user_b)
        return self.fetchall('''
            SELECT msg.data, reply.data
            FROM message as msg JOIN message as reply ON msg.id = reply.reply_to
            WHERE reply.user_id = ? AND msg.user_id = ?
        ''', [user_a, user_b])

    def user_messages(self, user):
        uid = self.get_user_id(user)
        for values in self.fetchall(f'SELECT data FROM message WHERE user_id = {uid} ORDER BY id'):
            yield values[0]

    def user_messages_count(self, user):
        uid = self.get_user_id(user)
        return self.fetchone(f'SELECT COUNT(*) FROM message WHERE user_id = {uid}')
