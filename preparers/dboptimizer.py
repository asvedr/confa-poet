import sqlite3
import re
import sys

base = sqlite3.connect(sys.argv[1])

def get_autr_id(name):
    try:
        return base.execute('SELECT id FROM user WHERE name = ?', (name, )).fetchone()[0]
    except TypeError:
        raise KeyError('User %s not found' % name)

def delete_autr(autr_id):
    base.execute('DELETE FROM user WHERE id = ?', [autr_id])

def select_list(request, *args):
    return [row[0] for row in base.execute(request, *args).fetchall()]

all_autrs = {row[0] for row in base.execute('''SELECT name FROM user''').fetchall()}

re_to_cut = [
    re.compile(r'\d+.\d+.\d+ \d+:\d+:\d+'),
    re.compile(r' via.*'),
]

ok_autrs = {
    autr for autr in all_autrs
    if not any([regex.search(autr) for regex in re_to_cut])
}
bad_autrs = set()
autrs_to_merge = {}

def clear_name(name):
    for regex in re_to_cut:
        name = regex.sub('', name)
    return name.strip()

for autr in all_autrs:
    clean = clear_name(autr)
    if clean == autr:
        continue
    if clean not in ok_autrs:
        bad_autrs.add(autr)
    else:
        autrs_to_merge[autr] = clean

for to_get, to_set in autrs_to_merge.items():
    id_get = get_autr_id(to_get)
    id_set = get_autr_id(to_set)
    base.execute('UPDATE message SET user_id = ? WHERE user_id = ?', [id_set, id_get])
    delete_autr(id_get)
    base.commit()
    print('MERGE "%s" => "%s" OK' % (to_get, to_set))

for autr in bad_autrs:
    autr_id = get_autr_id(autr)
    base.execute('DELETE FROM message WHERE user_id = ?', [autr_id])
    delete_autr(autr_id)
    base.commit()
    print('DELETE AUTOR("%s") WITH MESSAGES OK' % autr)

base.commit()
