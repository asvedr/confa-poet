import utils
import sys

base = utils.ConfaBase(sys.argv[1])

rename = {
    'Сережа Добриков': 'imtheonlyoneaaa',
    '🦋': 'supermrazota',
    # 'koyaanisqatsi', '',
    'Сережа Мартынов': 'brado_breey',
    'Mick Turturro': 'mickturturro',
    'Кирилл': 'kirill',
    'Саша': 'yourfriendleslie',
    'yourfriendLeslie': 'yourfriendleslie',
    'Саша Рогов': 'sasha_rogov',
    'Лера': 'ukikangaroo',
    'Олег Поздняков': 'lalsas',

    'Сеня😻': 'mickturturro',
    'Александр Йыщль': 'sasha_rogov',
    'Настоечка🌸': 'supermrazota',
    'Sergei Kalsarikännit': 'imtheonlyoneaaa',
    'Viracocha': 'brado_breey',
    'Velos Lesar': 'kirill',
    'корбен, детка': 'yourfriendleslie',
    'uki kangaroo': 'ukikangaroo',
}

# rename all
list(map(lambda args: base.rename(*args), rename.items()))

users_to_process = [
    'imtheonlyoneaaa',
    'supermrazota',
    'koyaanisqatsi',
    'brado_breey',
    'mickturturro',
    'kirill',
    'yourfriendleslie',
    'sasha_rogov',
    'ukikangaroo',
    'lalsas',
]


_RU = 'ё' + ''.join(chr(c) for c in range(ord('а'), ord('я') + 1))
RU = set(_RU + _RU.upper())
_EN = ''.join(chr(c) for c in range(ord('a'), ord('z') + 1))
EN = set(_EN + _EN.upper())
NUMS = set('0123456789')
PM = set('>+-')


def has_no_en(text):
    return not any(symbol in EN for symbol in text)


def has_no_nums(text):
    return not any(symbol in NUMS for symbol in text)


def has_no_plus_minus_at_begin(text):
    text = text.strip()
    return text and text[0] not in PM and text[-1] not in PM


def has_ru(text):
    return any(symbol in RU for symbol in text)


phrase_filters = [has_ru, has_no_en, has_no_nums, has_no_plus_minus_at_begin]


for user in users_to_process:
    with open('%s.phrases' % user, 'wt') as handler:
        for message in set(base.user_messages(user)):
            message = '\n'.join(filter(''.__ne__, message.split('.')))
            for phrase in message.split('\n'):
                if all(func(phrase) for func in phrase_filters):
                    handler.write(f'{phrase}\n')
