import sys
import os
import re
import json
import sqlite3

from bs4 import BeautifulSoup

USER = 'user'
MESSAGE = 'message'

class MessageParser(object):

    def __init__(self):
        self.dirpath = sys.argv[1]
        self.files = list(filter(
            lambda path: re.match('messages.*.html', path),
            os.listdir(self.dirpath)
        ))
        self.files.sort(
            key=lambda name: int('0' + name.split('messages')[1].split('.html')[0])
        )
        self.datetime_re = re.compile(r'\d+.\d+.\d+ \d+:\d+:\d+')
        self.base = sqlite3.connect(sys.argv[2] + '.db')
        self.init_base()

    def init_base(self):
        self.base.execute(f'''
            CREATE TABLE {USER} (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `name` TEXT NOT NULL
            )
        ''')
        self.base.execute(f'''
            CREATE TABLE {MESSAGE} (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `data` TEXT,
                `user_id` INTEGER NOT NULL,
                FOREIGN KEY (user_id) REFERENCES user(id)
            )
        ''')

    def drop_indexes(self):
        self.base.execute('DROP INDEX IF EXISTS USR_TG_INDEX')

    def add_indexes(self):
        self.base.execute(f'CREATE INDEX IF NOT EXISTS USR_TG_INDEX ON {USER} (`name`)')

    def process(self):
        id_attrs = {'message', 'default', 'clearfix'}
        classes_to_skip = [
            'media_wrap', 'forwarded', 'body', 'pull_right', 'pull_left', 'history',
            'userpic', 'initials', 'title', 'description', 'details', 'service'
        ]
        message_re = re.compile(r'message')
        go_to_message_re = re.compile(r'.*#go_to_message')
        current_message_id = None
        sender = None
        for file in self.files:
            with open(os.path.join(self.dirpath, file)) as handler:
                soup = BeautifulSoup(handler.read(), 'html.parser')
            divs = (
                soup.find('body')
                    .find('div', {'class': 'page_wrap'})
                    .find('div', {'class': 'page_body'}).findAll('div')
            )

            def process_message_body(text):
                if self.datetime_re.search(sender):
                    # Это форвард
                    return
                self.add_message(sender, text)

            for i in range(len(divs)): 
                div = divs[i]
                div_class = set(div.attrs.get('class'))
                if id_attrs.issubset(div_class):
                    current_message_id = int(message_re.sub('', div.attrs.get('id')))
                elif div_class == {'from_name'}: 
                    sender = div.text.strip()
                elif div_class == {'text'}:
                    process_message_body(div.text.strip())
                elif 'reply_to' in div_class:
                    pass
                elif 'media_wrap' in div_class or 'forwarded' in div_class:
                    pass
                elif any([class_ in div_class for class_ in classes_to_skip]):
                    pass
                else:
                    print('BAD BODY\n%s\n%s\n\n' % (div, div_class))
                reply_id = None
            # self.base.commit()
            print('FILE %s PROCESSED' % file)
            self.base.commit()

    def _get_or_create_user_id(self, username):
        row = self.base.execute(f'SELECT `id` FROM {USER} WHERE `name` = ?', [username]).fetchone()
        if row:
            return row[0]
        else:
            cursor = self.base.execute(f'INSERT INTO {USER} (`name`) VALUES (?)', [username])
            return cursor.lastrowid

    def add_message(self, sender, message):
        self.base.execute(f'''
            INSERT INTO {MESSAGE} (`user_id`, `data`)
            VALUES (?, ?)
        ''', [self._get_or_create_user_id(sender), message])

    def get_users(self):
        return [
            line[0]
            for line in self.base.execute(f'SELECT `name` FROM {USER}').fetchall()
        ]

    def get_messages_count(self):
        return self.base.execute(f'SELECT count(*) FROM {MESSAGE}').fetchone()[0]


parser = MessageParser()
parser.process()
print(parser.get_users())
print(parser.get_messages_count())
