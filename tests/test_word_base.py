from unittest import TestCase
from poet.base.word_base import WordBase, Word


class TestWordBase(TestCase):

    def setUp(self):
        self.base = WordBase(':memory:')
        self.strong_symbol = '`'
        self.words = [
            Word('ручка', 2, 'учка'),
            Word('нога', 2, 'а'),
            Word('томат', 2, 'ат'),
            Word('кот', 1, 'от'),
        ]

    def test_indexes(self):
        self.base.drop_indexes()
        self.base.add_words(self.words)
        self.base.set_indexes()

    def test_words(self):
        self.assertEqual(self.base.get_size(), 0)
        self.base.add_words(self.words)
        self.assertEqual(self.base.get_size(), 4)
        word = self.base.get_word_by_text('ручка')
        self.assertEqual(word, Word('ручка', 2, 'учка'))
        self.base.add_word(Word('ру', 2, 'у'))
        self.assertEqual(self.base.get_size(), 5)

    def test_conflict(self):
        self.assertEqual(self.base.get_size(), 0)
        self.base.add_word(Word('биба', 2, 'иба'))
        self.assertEqual(self.base.get_size(), 1)
        self.base.add_word(Word('биба', 2, 'а'))
        self.assertEqual(self.base.get_size(), 1)
        word = self.base.get_word_by_text('биба')
        self.assertEqual(word, Word('биба', 2, 'иба'))

    def test_get_syl_count_sum(self):
        self.base.add_words(self.words)
        self.assertEqual(
            (3, []),
            self.base.get_syl_count_sum(['нога', 'кот'])
        )
        self.assertEqual(
            (4, []),
            self.base.get_syl_count_sum(['нога', 'кот', 'кот'])
        )
        self.assertEqual(
            (4, ['бок', 'слизень', 'бок']),
            self.base.get_syl_count_sum(['нога', 'бок', 'кот', 'слизень', 'кот', 'бок'])
        )
