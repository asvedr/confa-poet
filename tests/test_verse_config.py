from unittest import TestCase
from poet.verse_config import VerseConfig, InvalidConfig, StaticRow, RangeRow, LinkedRow


class TestVerseConfig(TestCase):

    def test_validate(self):
        invalid_configs = [
            {},
            {'sizes': [1, 2], 'wow': 'anything'},
            {'sizes': 'boo'},
            {'sizes': [1, 2, 'l10']},
            {'sizes': [[], 1]},
            {'sizes': [1, [1, 2, 3]]},
            {'sizes': [1, [1]]},
            {'sizes': [1, 2], 'rhymes': [[0, 4]]},
            {'sizes': [1, 2], 'rhymes': [[-1, 1]]},
            {'sizes': [1, 2], 'rhymes': [{}]},
            {'sizes': [1, 2], 'rhymes': [0, 1]},
            {'sizes': [1, 2], 'rhymes': [[0, 1]], 'unrhymed': [-1]},
            {'sizes': [1, 2], 'rhymes': [[0, 1]], 'unrhymed': [4]},
            {'sizes': [1, 2], 'unrhymed_all': 1},
        ]
        valid_config = [
            {'sizes': [9, 8, 9, 8]},
            {'sizes': [9, 2], 'unrhymed_all': True},
            {'sizes': [1, 2, 3, 4, 5], 'rhymes': [[0, 2, 4], [1, 3]]},
            {'sizes': [1, 2, 3], 'rhymes': [[0, 1]], 'unrhymed': [2]},
            {'sizes': [[9, 12], [9, 12], 'l0', 'l1 + 2']}
        ]
        for config in invalid_configs:
            self.assertRaises(InvalidConfig, lambda: VerseConfig('a', config))
        for config in valid_config:
            VerseConfig('a', config)

    def test_rows_rhymes(self):
        config = {
            'sizes': [[9, 12], 8, 'l0-1', 'l1', 11],
            'rhymes': [[0, 1, 3]],
            'unrhymed': [4],
        }
        verse_config = VerseConfig('a', config)
        rows = [
            RangeRow([9, 12], has_rhyme=True, next_rhyme=1),
            StaticRow(8, has_rhyme=True, rhyme_of=0, next_rhyme=3),
            LinkedRow('l0 - 1', has_rhyme=True),
            LinkedRow('l1', has_rhyme=True, rhyme_of=1),
            StaticRow(11, has_rhyme=False)
        ]
        rows[0].tag_can_be_missed = True
        self.assertEqual(verse_config.rows, rows)

        config = {
            'sizes': [1, 2],
            'unrhymed_all': True,
        }
        rows = [StaticRow(1, has_rhyme=False), StaticRow(2, has_rhyme=False)]
        rows[0].tag_can_be_missed = True
        self.assertEqual(VerseConfig('a', config).rows, rows)

    def test_get_row_size(self):
        self.assertEqual(StaticRow(7).get_size([], False), 7)
        for _ in range(10):
            size = RangeRow([3, 5]).get_size([], False)
            self.assertTrue(size >= 3 and size <= 5)
        self.assertEqual(LinkedRow('l1').get_size([], False), None)
        self.assertEqual(LinkedRow('l1').get_size([5], False), None)
        self.assertEqual(LinkedRow('l1').get_size([5, 6], False), 6)
        self.assertEqual(LinkedRow('l0+2').get_size([5], False), 7)
        self.assertEqual(LinkedRow('l0-2').get_size([5], False), 3)

        self.assertEqual(StaticRow(3).get_size([], True), 3)
        self.assertEqual(RangeRow([3, 5]).get_size([], True), None)
        self.assertEqual(LinkedRow('l0').get_size([1], True), 1)
        self.assertEqual(LinkedRow('l0').get_size([], True), None)
