import os
from unittest import TestCase
from poet.base.word_base import Word
from poet.dict_loaders.loader import DictLoader


class TestLoader(TestCase):

    def setUp(self):
        self.loader = DictLoader()
        self.folder = os.path.dirname(__file__)

    def test_hagen(self):
        path = os.path.join(self.folder, 'test.hagen.txt')
        words = list(self.loader.load(path))
        src = 'успение успения успению успение успением успении успех успеха успеху успех успехом успехе'
        ethalon = set(src.split(' '))
        self.assertEqual(ethalon, {word.text for word in words})

    def test_forms(self):
        path = os.path.join(self.folder, 'test.forms.txt')        
        words = list(self.loader.load(path))
        src = 'абажурный абажурная абажурное абаз абазы абаза абазов абазу'
        ethalon = set(src.split(' '))
        self.assertEqual(ethalon, {word.text for word in words})

    def test_simple(self):
        path = os.path.join(self.folder, 'test.simple.txt')        
        words = list(self.loader.load(path))
        src = 'помню чудное мгновенье перед'
        ethalon = set(src.split(' '))
        self.assertEqual(ethalon, {word.text for word in words})

    def test_english_js(self):
        path = os.path.join(self.folder, 'test.js-eng-code.txt')
        words = list(self.loader.load(path))
        self.assertEqual(
            [
                Word("'cause", 1, 'AH0 Z'), Word('a', 1, 'AH0'), Word("a's", 1, 'EY1 Z'),
                Word('aaberg', 2, 'AA1 B ER0 G'), Word('aachen', 2, 'AA1 K AH0 N'), Word('aaker', 2, 'AA1 K ER0')
            ],
            words,
        )
