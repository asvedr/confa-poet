from unittest import TestCase
from poet.base.word_base import Word, WordBase
from poet.base.phrase_base import PhraseBase
from poet.base.phrase import Phrase, InvalidWord
from poet.langs.ru.processor import Processor as RuProcessor


class TestEmptyPhraseBase(TestCase):

    def make_word(self, raw: str):
        syl_count, ending = self.processor.get_syl_count_and_ending(raw)
        return Word(raw.replace(self.processor.strong_symbol, ''), syl_count, ending)

    def setUp(self):
        self.word_base = WordBase(':memory:')
        self.processor = RuProcessor('`')
        self.phrase_base = PhraseBase(':memory:', word_base=self.word_base, processor=self.processor)
        words = [
            Word('мгновенье', 3, 'эньэ'),
            Word('виденье', 3, 'эньэ'),
            Word('топал', 2, 'опал'),
            Word('хлопал', 2, 'опал'),
            Word('грохота`л', 3, 'ал'),
            Word('кал', 1, 'ал'),
            Word('сыпь', 1, 'ыпь'),
            Word('ходил', 2, 'ил'),
            Word('кеки', 2, 'еки'),
            Word('закопал', 3, 'опал'),
        ]
        self.word_base.add_words(words)
        self.word_base.commit()
        self.phrases = [
            'я помню чудное мгновенье',
            'как мимолетное виденье',
            'ногами топал',
            'дверью хлопал',
            'руками хлопал',
            'выпала сыпь',
            'по лесу ходил',
            'кеки',
            'закопал',
        ]
        self.rhymed_phrases = [
            'я помню чудное мгновенье',
            'как мимолетное виденье',
            'ногами топал',
            'дверью хлопал',
            'руками хлопал',
            'закопал',
        ]
        self.not_rhymed_phrases = [
            'выпала сыпь',
            'по лесу ходил',
            'кеки',
        ]

    def test_prepare_phrase_regular(self):
        ok_words = 'Я помню чудное мгновенье'
        phrase = self.phrase_base.prepare_phrase(ok_words, {})
        must_be = Phrase('япомнючудноемгновенье', ok_words, 'эньэ', 'мгновенье', 9, [])
        self.assertTrue(must_be.check_eq(phrase))

    def test_prepare_phrase_predict(self):
        ok_words = 'Я помню как'
        phrase = self.phrase_base.prepare_phrase(ok_words, {})
        must_be = Phrase('япомнюкак', ok_words, 'ак', 'как', 4, [])
        self.assertTrue(must_be.check_eq(phrase))

    def test_prepare_phrase_invalid(self):
        ok_words = 'Я помню кака'
        self.assertRaises(
            InvalidWord,
            lambda: self.phrase_base.prepare_phrase(ok_words, {})
        )

    def test_prepare_phrase_euristic_cut(self):
        ok_words = 'Я помню как'
        phrase = self.phrase_base.prepare_phrase(ok_words, {'min_syl_count': 7})
        self.assertEqual(phrase, None)
        phrase = self.phrase_base.prepare_phrase(ok_words, {'max_syl_count': 1})
        self.assertEqual(phrase, None)
        params = {'min_syl_count': 1, 'max_syl_count': 7}
        phrase = self.phrase_base.prepare_phrase(ok_words, params)
        must_be = Phrase('япомнюкак', ok_words, 'ак', 'как', 4, [])
        self.assertTrue(must_be.check_eq(phrase))

    def test_add_words(self):
        phrase_base = self.phrase_base
        self.assertEqual(phrase_base.get_size(), 0)
        phrase_base.add_phrases([
            phrase_base.prepare_phrase(phrase, {})
            for phrase in self.phrases
        ])
        self.assertEqual(phrase_base.get_size(), len(self.phrases))


class TestFullPhraseBase(TestCase):

    def setUp(self):
        TestEmptyPhraseBase.setUp(self)
        self.phrase_base.add_phrases(
            self.phrase_base.prepare_phrase(phrase, {})
            for phrase in self.phrases
        )
        self.phrase_base.recalculate_rhymes()        

    def test_recalculate_rhymes(self):
        phrase_base = self.phrase_base

        phrase_a = phrase_base.get_phrase_by_text('я помню чудное мгновенье')
        phrase_b = phrase_base.get_phrase_by_text('как мимолетное виденье')

        query = 'SELECT rhyme_id, rhyme_syl_count FROM rhymes WHERE phrase_id = ?'

        result = phrase_base.fetchall(query, [phrase_a.slug])
        self.assertEqual(result, [(phrase_b.slug, phrase_b.syl_count)])
        result = phrase_base.fetchall(query, [phrase_b.slug])
        self.assertEqual(result, [(phrase_a.slug, phrase_a.syl_count)])

        phrase_a = phrase_base.get_phrase_by_text('ногами топал')
        phrase_b = phrase_base.get_phrase_by_text('дверью хлопал')
        phrase_c = phrase_base.get_phrase_by_text('руками хлопал')
        phrase_d = phrase_base.get_phrase_by_text('закопал')
        single_phrase = phrase_base.get_phrase_by_text('выпала сыпь')

        self.assertEqual(phrase_base.fetchall(query, [single_phrase.slug]), [])

        result = phrase_base.fetchall(query, [phrase_a.slug])
        self.assertEqual(len(result), 3)
        self.assertEqual(
            set(result),
            {
                (phrase_b.slug, phrase_b.syl_count),
                (phrase_c.slug, phrase_c.syl_count),
                (phrase_d.slug, phrase_d.syl_count),
            }
        )

        self.assertEqual(
            set(phrase_base.fetchall(query, [phrase_b.slug])),
            {(phrase_a.slug, phrase_a.syl_count), (phrase_d.slug, phrase_d.syl_count)}
        )

    def test_get_random_rhymed(self):
        unused_keys = set(self.rhymed_phrases)
        used = []
        for _ in self.rhymed_phrases:
            phrase = self.phrase_base.get_random_rhymed(exclude=used)
            self.assertTrue(phrase)
            self.assertTrue(phrase not in used)
            self.assertTrue(phrase.text in unused_keys)
            unused_keys.remove(phrase.text)
            used.append(phrase)
        self.assertEqual(unused_keys, set())
        self.assertEqual({phrase.text for phrase in used}, set(self.rhymed_phrases))

        phrase = self.phrase_base.get_random_rhymed(syl_count=3)
        self.assertEqual(phrase.text, 'закопал')

    def test_get_random_rhymed_predict(self):
        phrase = self.phrase_base.get_random_rhymed(rhyme_syl_count=3, syl_count=4)
        self.assertEqual(phrase.text, 'дверью хлопал')
        phrase = self.phrase_base.get_random_rhymed(rhyme_syl_count=16)
        self.assertEqual(phrase, None)

    def test_get_random_unrhymed(self):
        # Check getting totally unrhymed
        unrhymed = self.phrase_base.get_random_unrhymed(syl_count=2)
        self.assertTrue(unrhymed and unrhymed.text == 'кеки')

        possible_phrases_9s = [
            'я помню чудное мгновенье',
            'как мимолетное виденье',
        ]
        phrase = self.phrase_base.get_random_unrhymed(syl_count=9, exclude=[unrhymed])
        self.assertTrue(phrase and phrase.text in possible_phrases_9s)

        phrase = self.phrase_base.get_random_unrhymed(
            syl_count=9,
            exclude=[unrhymed, phrase],
        )
        self.assertTrue(phrase is None)

    def test_get_rhyme(self):
        phrase = self.phrase_base.get_phrase_by_text('дверью хлопал')
        rhyme = self.phrase_base.get_rhyme(phrase, syl_count=3)
        self.assertEqual(rhyme.text, 'закопал')
        rhyme = self.phrase_base.get_rhyme(phrase, new_rhyme_syl_count=3)
        self.assertEqual(rhyme.text, 'ногами топал')

        phrase = self.phrase_base.get_phrase_by_text('я помню чудное мгновенье')
        rhyme = self.phrase_base.get_rhyme(phrase)
        self.assertEqual(rhyme.text, 'как мимолетное виденье')
        new_rhyme = self.phrase_base.get_rhyme(phrase, exclude=[rhyme])
        self.assertEqual(new_rhyme, None)

    def test_change_end(self):
        phrase = self.phrase_base.get_phrase_by_text('я помню чудное мгновенье')
        self.assertEqual(phrase.end, 'эньэ')

        self.phrase_base.change_end(phrase.text, 'ащащ')
        self.phrase_base.recalculate_rhymes()
        phrase = self.phrase_base.get_phrase_by_text('я помню чудное мгновенье')
        self.assertEqual(phrase.end, 'ащащ')
        self.assertEqual(self.phrase_base.get_rhyme(phrase), None)

    def test_change_last_word(self):
        phrase = self.phrase_base.get_phrase_by_text('я помню чудное мгновенье')
        self.assertEqual(phrase.last_word, 'мгновенье')

        self.phrase_base.change_last_word(phrase.text, 'ащащ')
        self.phrase_base.recalculate_rhymes()
        phrase = self.phrase_base.get_phrase_by_text('я помню чудное мгновенье')
        self.assertEqual(phrase.last_word, 'ащащ')

    def test_tags(self):
        phrase = self.phrase_base.get_phrase_by_text('я помню чудное мгновенье')
        tags = self.phrase_base.get_phrase_tags(phrase)
        self.assertEqual({'я', 'помню', 'чудное', 'мгновенье'}, set(tags))
        phrase = self.phrase_base.get_phrase_by_text('дверью хлопал')
        tags = self.phrase_base.get_phrase_tags(phrase)
        self.assertEqual({'дверью', 'хлопал'}, set(tags))

    def test_get_rhyme_with_tags(self):
        phrase = self.phrase_base.get_random_rhymed(tags=['топал'])
        self.assertEqual(phrase.text, 'ногами топал')
        phrase = self.phrase_base.get_random_rhymed(tags=['сыпь'])
        self.assertEqual(phrase, None)

    def test_get_unrhyme_with_tags(self):
        phrase = self.phrase_base.get_random_unrhymed(tags=['топал'])
        self.assertEqual(phrase.text, 'ногами топал')
        phrase = self.phrase_base.get_random_unrhymed(tags=['сыпь'])
        self.assertEqual(phrase.text, 'выпала сыпь')
