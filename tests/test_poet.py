from unittest import TestCase
from poet.base.word_base import WordBase, Word
from poet.base.phrase_base import PhraseBase
from poet.verse_config import VerseConfig
from poet.poet import Poet, GenerationFailed
from poet.langs.ru.processor import Processor as RuProcessor


class TestPoet(TestCase):

    def setUp(self):
        word_base = WordBase(':memory:')
        self.phrase_base = PhraseBase(
            ':memory:',
            word_base=word_base,
            processor=RuProcessor('`'),
        )
        words = [
            Word('мгновенье', 3, 'эньэ'),
            Word('виденье', 3, 'эньэ'),
            Word('топал', 2, 'опал'),
            Word('хлопал', 2, 'опал'),
            Word('грохота`л', 3, 'ал'),
            Word('кал', 1, 'ал'),
            Word('сыпь', 1, 'ыпь'),
            Word('ходил', 2, 'ил'),
            Word('кеки', 2, 'еки'),
            Word('закопал', 3, 'опал'),
        ]
        word_base.add_words(words)
        word_base.commit()
        phrases = [
            'я помню НЕ чудное мгновенье',
            'как мимолетное виденье',
            'ногами топал',
            'дверью хлопал',
            'руками хлопал',
            'выпала сыпь',
            'по лесу ходил',
            'кеки',
            'закопал',
        ]
        self.phrase_base.add_phrases([
            self.phrase_base.prepare_phrase(phrase, {})
            for phrase in phrases
        ])
        self.phrase_base.recalculate_rhymes()
        self.verse_schemas = [
            VerseConfig(
                'rhymed',
                {'sizes': [10, 4, 9, 3], 'rhymes': [[0, 2], [1, 3]]},
            ),
            VerseConfig(
                'unrhymed',
                {'sizes': [2, 3], 'unrhymed_all': True},
            ),
            VerseConfig(
                'inpossible',
                {'sizes': [5, 1, 20]},
            )
        ]

    def test_create(self):
        Poet(self.phrase_base, self.verse_schemas)

    def test_gen_unrhymed_verse(self):
        poet = Poet(self.phrase_base, self.verse_schemas, retry_count=0)
        schema = [s for s in self.verse_schemas if s.name == 'unrhymed'][0]
        verse = poet.gen_verse(schema)
        self.assertEqual(verse, 'кеки\nзакопал')

    def test_gen_rhymed_verse(self):
        poet = Poet(self.phrase_base, self.verse_schemas, retry_count=0)
        schema = [s for s in self.verse_schemas if s.name == 'rhymed'][0]
        verse = poet.gen_verse(schema)
        self.assertEqual(
            verse,
            'я помню НЕ чудное мгновенье\n'
            'дверью хлопал\n'
            'как мимолетное виденье\n'
            'закопал'
        )

    def test_gen_verse_error(self):
        poet = Poet(self.phrase_base, self.verse_schemas, retry_count=10)
        schema = [s for s in self.verse_schemas if s.name == 'inpossible'][0]
        self.assertRaises(GenerationFailed, lambda: poet.gen_verse(schema))

    def test_gen_all(self):
        poet = Poet(self.phrase_base, self.verse_schemas, retry_count=10)
        verses = poet.gen_all_schemas()
        a = (
            'я помню НЕ чудное мгновенье\n'
            'дверью хлопал\n'
            'как мимолетное виденье\n'
            'закопал'
        )
        b = 'кеки\nзакопал'
        self.assertEqual(set(verses), {('rhymed', a), ('unrhymed', b)})

    def test_gen_random(self):
        poet = Poet(self.phrase_base, self.verse_schemas, retry_count=10)
        verse = poet.gen_random_verse()
        a = (
            'я помню НЕ чудное мгновенье\n'
            'дверью хлопал\n'
            'как мимолетное виденье\n'
            'закопал'
        )
        b = 'кеки\nзакопал'
        self.assertTrue(verse in {a, b})

    def test_uppercase(self):
        config = VerseConfig(
            'unrhymed',
            {'sizes': [2, 3], 'unrhymed_all': True},
        )
        poet = Poet(self.phrase_base, [config], retry_count=10)
        verse = poet.gen_random_verse(upper_first_symbol=True)
        b = 'Кеки\nЗакопал'
        self.assertEqual(verse, b)
