from unittest import TestCase
from poet.langs.ru.utils import reduce_word, ALPHABET
from poet.utils import split_to_words
from poet.langs.ru.processor import Processor


class TestUtils(TestCase):

    def test_reducer(self):
        self.assertEqual(reduce_word('ее'), 'ее')
        self.assertEqual(reduce_word('длиииинный'), 'длинный')

    def test_splitter(self):
        words = split_to_words('Аааа! так вот, ты какой))', ALPHABET)
        self.assertEqual(
            words,
            ['аааа', 'так', 'вот', 'ты', 'какой'],
        )
        words = Processor('').split_phrase_to_words('Аааа! так вот, ты какой))')
        self.assertEqual(
            words,
            ['а', 'так', 'вот', 'ты', 'какой'],
        )
