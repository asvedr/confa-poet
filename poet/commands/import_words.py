import argparse
import os

from poet.base.word_base import WordBase
from poet.dict_loaders.loader import DictLoader
from poet.dict_loaders.errors import InvalidWordInDict
from poet.commands.common import ParamError, command, batch_iterator


def check_extension(path, available_extensions):
    for ext in available_extensions:
        if path.endswith(ext):
            return True
    return False


def get_input_files(path, available_extensions):
    if os.path.isfile(path) and check_extension(path, available_extensions):
        return [path]
    if os.path.isdir(path):
        files = os.listdir(path)
        files = [os.path.join(path, file) for file in files]
        return [
            file
            for file in files
            if os.path.isfile(file) and check_extension(file, available_extensions)
        ]
    raise ParamError('Invalid input path')


@command
def main():
    loader = DictLoader()
    description = (
        'Create or append word database. '
        'Can import file or directory. '
        f'Supported extensions {loader.extensions}'
    )
    parser = argparse.ArgumentParser(description)
    parser.add_argument('input', help='Input file or folder')
    parser.add_argument('output', help='Output file. Base will be created or appended')
    parser.add_argument('--batch', help='batch_size', default='1000')
    args = parser.parse_args()
    try:
        batch_size = int(args.batch)
        if batch_size < 1:
            raise ValueError
    except ValueError:
        raise ParamError('batch must be int >= 1')

    word_base = WordBase(args.output)
    print(f'Base size before: {word_base.get_size()}')
    input_files = get_input_files(args.input, loader.extensions)
    input_files.sort()
    word_base.drop_indexes()
    for filepath in input_files:
        print(f'Processing {filepath}')
        try:
            for batch in batch_iterator(loader.load(filepath), batch_size):
                word_base.add_words(batch)
                word_base.commit()
            print('Done')
        except InvalidWordInDict as error:
            print(f'Invalid word: "{error.word}"')
    word_base.set_indexes()
    print(f'Base size after: {word_base.get_size()}')
