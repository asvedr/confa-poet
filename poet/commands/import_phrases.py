import argparse
import os
import sys
from typing import Optional, Set, Dict, Any

from poet.base.word_base import WordBase
from poet.base.phrase_base import PhraseBase
from poet.base.phrase import InvalidWord
from poet.commands.common import ParamError, command, batch_iterator
from poet.langs.processors import PROCESSORS


def prepare_lines(phrase_base, lines, euristic_params):
    phrases = []
    invalid_words = []
    for line in lines:
        if not line.strip():
            continue
        try:
            phrase = phrase_base.prepare_phrase(line, euristic_params)
            if phrase:
                phrases.append(phrase)
        except InvalidWord as error:
            invalid_words.append(error.word)
    return phrases, invalid_words


class InvalidWordsHandler(object):
    def __init__(self, path):
        if path == '<stdout>':
            self.path = None
            self.handler = sys.stdout
        else:
            self.path = path
            self.handler = None

    def __enter__(self, *args, **kwargs):
        if self.path:
            self.handler = open(self.path, 'wt')
        return self.handler

    def __exit__(self, *args, **kwargs):
        if not self.path:
            return
        self.handler.close()


def get_blacklist(path: Optional[str]) -> Set[str]:
    if path is None:
        return set()
    with open(path) as handler:
        lines = map(str.strip, handler)
        return set(filter(lambda line: line, lines))


def make_euristic_params(args) -> Dict[str, Any]:
    params = {}
    if args.min_syl_count is not None:
        try:
            params['min_syl_count'] = int(args.min_syl_count)
        except:
            raise ParamError('min_syl_count is not int')
    if args.max_syl_count is not None:
        try:
            params['max_syl_count'] = int(args.max_syl_count)
        except:
            raise ParamError('max_syl_count is not int')
    blacklist = get_blacklist(args.ebp)
    params['ending_blacklist'] = blacklist
    return params


@command
def main():
    description = (
        'Create or append phrase database. '
        'Input must be single text file. '
        'One line is one phrase.'
    )
    parser = argparse.ArgumentParser(description)
    parser.add_argument('input', help='Input file or folder')
    parser.add_argument('word_base', help='Path to word base file')
    parser.add_argument('lang', help=f'Language', choices=list(PROCESSORS.keys()))
    parser.add_argument('output', help='Output file. Base will be created or appended')
    parser.add_argument('--batch', help='Batch size', default='1000')
    parser.add_argument(
        '--invalid_words',
        help='where to write invalid_words. Filepath or <stdout>',
        default='<stdout>',
    )
    parser.add_argument(
        '--min_syl_count',
        help='Minimum syllable count in phrase',
        default=None,
    )
    parser.add_argument(
        '--max_syl_count',
        help='Maximum syllable count in phrase',
        default=None,
    )
    parser.add_argument(
        '--ebp',
        help='Ending blacklist path',
        default=None,
    )
    args = parser.parse_args()
    try:
        batch_size = int(args.batch)
        if batch_size < 1:
            raise ValueError
    except ValueError:
        raise ParamError('batch must be int >= 1')
    euristic_params = make_euristic_params(args)
    word_base = WordBase(args.word_base)
    if not os.path.isfile(args.input):
        raise ParamError('Invalid input')
    processor = PROCESSORS[args.lang]()
    phrase_base = PhraseBase(args.output, word_base=word_base, processor=processor)
    print(f'Base size before: {phrase_base.get_size()}')
    phrase_base.drop_indexes()
    with open(
        args.input
    ) as handler, InvalidWordsHandler(
        args.invalid_words
    ) as invalid_words_handler:
        invalid_words_set = set()
        processed = 0
        for lines in batch_iterator(handler, batch_size):
            prepared, invalid_words = prepare_lines(phrase_base, lines, euristic_params)
            phrase_base.add_phrases(prepared)
            phrase_base.commit()
            for word in invalid_words:
                invalid_words_set.add(word)
            processed += len(lines)
            print(f'PROCESSED: {processed}')
        for word in invalid_words_set:
            invalid_words_handler.write(f'{word}\n')
    phrase_base.recalculate_rhymes()
    phrase_base.set_indexes()
    print(f'Base size after: {phrase_base.get_size()}')
