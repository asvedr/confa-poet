from functools import wraps
import sys


class ParamError(Exception):
    def __init__(self, message):
        self.message = message


def command(func):
    @wraps(func)
    def wrapper():
        try:
            func()
        except ParamError as error:
            print(error.message)
            sys.exit(1)
    return wrapper


def batch_iterator(iterator, batch_size):
    batch = []
    for item in iterator:
        batch.append(item)
        if len(batch) >= batch_size:
            yield batch
            batch = []
    if batch:
        yield batch
