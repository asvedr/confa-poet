import argparse
import os
import sys

from poet.base.word_base import WordBase, Word
from poet.commands.common import ParamError, command, batch_iterator


@command
def main():
    description = 'View or change ending of word in word base'
    parser = argparse.ArgumentParser(description)
    parser.add_argument('word_base', help='Path to database')
    parser.add_argument('text', help='Word text')
    parser.add_argument('-e', '--ending', help='New ending. If param doesn\'t set script just show you old ending')

    args = parser.parse_args()

    db = WordBase(args.word_base)
    word = db.get_word_by_text(args.text)
    if word is None:
        print('Phrase not found')
        sys.exit(1)
    print(f'Text: "{word.text}", Size:{word.syl_count}, Ending: "{word.end}"')
    if args.ending:
        db.change_end(args.text, args.ending)
        db.commit()
        print(f'Set ending "{args.ending}"')
