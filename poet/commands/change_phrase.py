import argparse
import os
import sys

from poet.base.phrase_base import PhraseBase
from poet.base.phrase import Phrase
from poet.commands.common import ParamError, command, batch_iterator


@command
def main():
    description = 'View or change ending of phrase in phrase base'
    parser = argparse.ArgumentParser(description)
    parser.add_argument('phrase_base', help='Path to database')
    parser.add_argument('text', help='Phrase text')
    parser.add_argument('-e', '--ending', help='New ending. If param doesn\'t set script just show you old ending')
    parser.add_argument('-w', '--word', help='New last word. If param doesn\'t set script just show you old last word')

    args = parser.parse_args()

    db = PhraseBase(args.phrase_base)
    phrase = db.get_phrase_by_text(args.text)
    if phrase is None:
        print('Phrase "%s" not found' % args.text)
        sys.exit(1)
    print(f'Text: "{phrase.text}", Size:{phrase.syl_count}, Ending: "{phrase.end}", Last word: {phrase.last_word}')
    if args.ending:
        db.change_end(args.text, args.ending)
        print(f'Set ending "{args.ending}"')
    if args.word:
        db.change_last_word(args.text, args.word)
        print(f'Set last word "{args.word}"')
    if args.ending or args.word:
        print('Recalculating rhymes')
        db.recalculate_rhymes()
