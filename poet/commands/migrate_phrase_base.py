import argparse
import os

from poet import WordBase
from poet.base.phrase_base import PhraseBase
from poet.base.legacy_phrase_base import LegacyPhraseBase
from poet.dict_loaders.loader import DictLoader
from poet.dict_loaders.errors import InvalidWordInDict
from poet.langs.processors import PROCESSORS
from poet.commands.common import ParamError, command, batch_iterator


@command
def main():
    loader = DictLoader()
    description = (
        'Create or append word database. '
        'Can import file or directory. '
        f'Supported extensions {loader.extensions}'
    )
    parser = argparse.ArgumentParser(description)
    parser.add_argument('input', help='Old phrase base')
    parser.add_argument('output', help='New phrase base')
    parser.add_argument('lang', help='Language', choices=list(PROCESSORS.keys()))
    parser.add_argument('--batch', help='batch_size', default='1000')
    args = parser.parse_args()
    try:
        batch_size = int(args.batch)
        if batch_size < 1:
            raise ValueError
    except ValueError:
        raise ParamError('batch must be int >= 1')
    processor = PROCESSORS[args.lang]()
    word_base = WordBase(':memory:')
    old_base = LegacyPhraseBase(args.input)
    new_base = PhraseBase(args.output, word_base=word_base, processor=processor)
    print(f'Source base size: {old_base.get_size()}')
    print(f'Destination base size: {new_base.get_size()}')
    phrases = old_base.read_all(batch_size)
    new_base.drop_indexes()
    for phrase_batch in batch_iterator(phrases, batch_size):
        for phrase in phrase_batch:
            words = new_base.processor.split_phrase_to_words(phrase.text)
            phrase.tags = new_base.processor.get_tags(words)
        new_base.add_phrases(phrase_batch)
    new_base.recalculate_rhymes()
    new_base.set_indexes()
    print(f'Destination base size: {new_base.get_size()}')
