import collections
from functools import lru_cache
import random
import re
from typing import List, Union

import trafaret as T


class InvalidConfig(Exception):

    def __init__(self, name, message):
        self.name = name
        self.message = message

    def __str__(self):
        return f'InvalidConfig({self.name}): {self.message}'

    __repr__ = __str__


class ProtoRow(object):

    def __init__(self, has_rhyme=True, rhyme_of=None, next_rhyme=None, tag_can_be_missed=False):
        self.has_rhyme = has_rhyme
        # Row index or None
        self.rhyme_of = rhyme_of
        # Row index or None
        self.next_rhyme = next_rhyme
        self.tag_can_be_missed = tag_can_be_missed

    def get_size(self, row_syl_counts: List[int], as_rhyme: bool):
        raise NotImplemented()

    def _fields(self):
        return {
            key: val
            for key, val in self.__dict__.items()
            if not key.startswith('_')
        }

    def __repr__(self):
        return f'{type(self).__name__}({self._fields()})'

    def __eq__(self, other):
        return type(self) == type(other) and self._fields() == other._fields()


class StaticRow(ProtoRow):

    def __init__(self, value: int, **kwargs):
        super().__init__(**kwargs)
        self.value = value

    def get_size(self, _: List[int], as_rhyme: bool):
        return self.value


class RangeRow(ProtoRow):

    def __init__(self, rng: List[int], **kwargs):
        super().__init__(**kwargs)
        min_val, max_val = rng
        self.min_val = min_val
        self.max_val = max_val

    def get_size(self, _: List[int], as_rhyme: bool):
        if not as_rhyme:
            return random.randint(self.min_val, self.max_val)


class LinkedRow(ProtoRow):

    def __init__(self, code: str, **kwargs):
        super().__init__(**kwargs)
        if '+' in code:
            line, coef = code.split('+')
            self._prepare(line, coef, 1)
        elif '-' in code:
            line, coef = code.split('-')
            self._prepare(line, coef, -1)
        else:
            self._prepare(code, '0', 1)

    def _prepare(self, line_src: str, coef_src: str, coef_mul: str):
        self.line_index = int(line_src.split('l')[1].strip())
        self.coef = int(coef_src.strip()) * coef_mul

    def get_size(self, row_syl_counts: List[int], as_rhyme: bool):
        if self.line_index < len(row_syl_counts):
            return row_syl_counts[self.line_index] + self.coef


class VerseConfig(object):
    """ Typical verse config is look like:
        "style1: {
            "sizes": [9, 8, 9, 8],
            "rhymes": [[0, 2], [1, 3]]
        },
        ...
        "style2": {
            "sizes": [[9, 12], [9, 12], "l0", "l1 - 2"],
            "rhymed": [[1, 3]],
            "unrhymed": [0, 2]
        },
        ...
        "style3": {
            "sizes": [9, 9, 9, 9],
            "unrhymed_all": true
        }
    """

    row_re = re.compile(r'^l\d+( *(\+|\-) *\d+)?$')

    @classmethod
    @lru_cache(maxsize=1)
    def validator(cls):
        line_validator = (
            T.Int() | # number
            T.List(T.Int(), max_length=2, min_length=2) | # range
            T.String() # inherit from other line: "l2"
        )
        rhyme_validator = T.List(T.Int())
        return T.Dict(
            T.Key('sizes', trafaret=T.List(line_validator)),
            T.Key('rhymes', trafaret=T.List(rhyme_validator), default=[]),
            T.Key('unrhymed', trafaret=T.List(T.Int), default=[]),
            T.Key('unrhymed_all', trafaret=T.Bool(), default=False),
        )


    def __init__(self, name, config):
        try:
            config = self.validator().check(config)
        except T.dataerror.DataError as error:
            raise InvalidConfig(name, f'Invalid json: {str(error)}')
        self.name = name
        row_count = len(config['sizes'])
        self.rows = [
            self._prepare_row(line, row_count)
            for line in config['sizes']
        ]
        self.rows[0].tag_can_be_missed = True

        if config['unrhymed_all']:
            for row in self.rows:
                row.has_rhyme = False
            return

        for sequence in config['rhymes']:
            self._configurate_rhyme_sequence(sequence)
        for index in config['unrhymed']:
            if not (0 <= index < len(self.rows)):
                raise InvalidConfig(name, f'Row index {index} is out of range')
            self.rows[index].has_rhyme = False

    def _prepare_row(
            self,
            source: Union[str, int, List[int]],
            row_count: int
    ) -> ProtoRow:
        if isinstance(source, str):
            if not self.row_re.match(source):
                raise InvalidConfig(self.name, f'Invalid row string: {repr(source)}')
            row = LinkedRow(source)
            if not (0 <= row.line_index < row_count):
                raise InvalidConfig(self.name, f'Row {repr(source)} is out of range')
            return row
        elif isinstance(source, int):
            return StaticRow(source)
        elif isinstance(source, list):
            return RangeRow(source)

    def _configurate_rhyme_sequence(self, sequence: List[int]):
        for i in sequence:
            if not (0 <= i < len(self.rows)):
                raise InvalidConfig(self.name, f'Rhyme index {i} is out of row range')
        row_indexes = list(set(sequence))
        row_indexes.sort()
        for i in range(1, len(row_indexes)):
            row_index = row_indexes[i]
            previous_row_index = row_indexes[i - 1]
            row = self.rows[row_index]
            previous_row = self.rows[previous_row_index]
            previous_row.next_rhyme = row_index
            row.rhyme_of = previous_row_index
