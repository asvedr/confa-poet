import functools
from typing import List, Set


def retry_func(exceptions, count=10):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            i = 0
            while True:
                try:
                    return func(*args, **kwargs)
                except exceptions:
                    if i >= count:
                        raise
                    else:
                        i += 1
        return wrapper
    return decorator


class EuristicError(Exception):
    pass


def euristic_exclude_decorator(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except EuristicError:
            return
    return wrapper


def check_euristics(euristics, data, params):
    for euristic in euristics:
        if euristic(data, params):
            raise EuristicError()


def split_to_words(sentence: str, alphabet: Set[str]) -> List[str]:
    sentence = sentence.lower()
    word = ''
    result = []
    for sym in sentence:
        if sym in alphabet:
            word += sym
        elif len(word) > 0:
            result.append(word)
            word = ''
    if len(word) > 0:
        result.append(word)
    return result
