from abc import ABC, abstractmethod
from typing import Any, Callable, Dict, Optional, List, Tuple

from poet.base.word_base import Word


PrewordEuristic = Callable[[List[str], Dict[str, Any]], bool]
WordEuristic = Callable[[List[Word], Dict[str, Any]], bool]


def _exclude_empty(pre_words: List[str], _: Dict[str, Any]) -> bool:
    return len(pre_words) == 0


def _last_word_in_blacklist(pre_words: List[str], params: Dict[str, Any]) -> bool:
    return (
        'ending_blacklist' in params and
        pre_words[-1].lower() in params['ending_blacklist']
    )


def _exclude_bad_syl_count(words: List[Word], params: Dict[str, Any]) -> bool:
    syl_count = sum(word.syl_count for word in words)
    min_syl_count = params.get('min_syl_count')
    if min_syl_count is not None and syl_count < min_syl_count:
        return True
    max_syl_count = params.get('max_syl_count')
    if max_syl_count is not None and syl_count > max_syl_count:
        return True


class WordProcessor(ABC):

    @abstractmethod
    def get_syl_count(self, word: str) -> int:
        """ :word: single word in lowercase
            :return: syl count """
        raise NotImplementedError()

    @abstractmethod
    def get_syl_count_and_ending(self, word: str) -> Optional[Tuple[int, str]]:
        """ :word: single word in lowercase
            :return: (syl count, ending) """
        raise NotImplementedError()

    @abstractmethod
    def split_phrase_to_words(self, phrase: str) -> List[str]:
        """ :phrase: phrase in lowercase
            :return: list of words """
        raise NotImplementedError()

    def get_tags(self, words: List[str]) -> List[str]:
        """ Canonical variant is language specific stemming
            Default variant is just return words
        """
        return words

    def guess_word(self, text: str) -> Optional[Word]:
        """ :text: word that was not found in dict
            :return: predicted word """
        return None

    def guess_syl_count(self, text: str) -> Optional[int]:
        """ :text: word that was not found in dict
            :return: predicted word """
        return None

    def prewords_euristics(self) -> List[PrewordEuristic]:
        return [_exclude_empty, _last_word_in_blacklist]

    def before_ending_euristics(self) -> List[WordEuristic]:
        return [_exclude_bad_syl_count]

    def after_ending_euristics(self) -> List[WordEuristic]:
        return []
