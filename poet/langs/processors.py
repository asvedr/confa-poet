from poet.langs.ru.processor import Processor as RuProcessor
from poet.langs.en.processor import Processor as EnProcessor


PROCESSORS = {
    'ru': RuProcessor,
    'en': EnProcessor,
}
