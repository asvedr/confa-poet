from typing import Optional, List, Tuple
from poet.langs.proto import WordProcessor, PrewordEuristic, WordEuristic
from poet.utils import split_to_words


AVAILABLE_SYMBOLS = (
    {chr(i) for i in range(ord('a'), ord('z') + 1)} |
    {"'"}
)


class Processor(WordProcessor):

    def get_syl_count(self, word: str) -> int:
        raise NotImplementedError()

    def get_syl_count_and_ending(self, word: str) -> Optional[Tuple[int, str]]:
        raise NotImplementedError()

    def split_phrase_to_words(self, phrase: str) -> List[str]:
        return split_to_words(phrase, AVAILABLE_SYMBOLS)
