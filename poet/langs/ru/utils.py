from typing import List
from itertools import groupby


_VOWELS = 'уеыаоэяиюё'
ALWAYS_STRONG_VOWEL = 'ё'
VOWELS = {c for c in _VOWELS}
VOWELS_RHYME_MAP = {
    'ю': 'у',
    'е': 'э',
    'я': 'а',
    'ы': 'и',
    'ё': 'о',
}
CONSONANTS = {chr(c) for c in range(ord('а'), ord('я')) if chr(c) not in VOWELS}
ALPHABET = VOWELS | CONSONANTS


def reduce_word(word: str) -> str:
    """ remove dublicate of symbols. Example: прииивет => привет """
    result = ''
    for _, chars in groupby(list(word)):
        chars = list(chars)
        if len(chars) > 2:
            result += chars[0]
        else:
            result += ''.join(chars)
    return result
