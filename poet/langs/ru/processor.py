from typing import Any, Dict, List, Optional, Tuple

try:
    from simple_phrase_trigger.stemmer_ru import Stemmer as StemmerClass
except ImportError:
    StemmerClass = None

from poet.base.word_base import Word, StrongVowelNotFound
from poet.langs.proto import WordProcessor, PrewordEuristic, WordEuristic
from poet.langs.ru.utils import VOWELS, VOWELS_RHYME_MAP, ALWAYS_STRONG_VOWEL, ALPHABET, reduce_word
from poet.utils import split_to_words


def normalize_ending_vowels(text: str) -> str:
    for k, v in VOWELS_RHYME_MAP.items():
        text = text.replace(k, v)
    return text


def estimate_strong_vowel(text: str, vowel_indexes: List[int]) -> int:
    if len(vowel_indexes) == 1:
        return vowel_indexes[0]
    for i in vowel_indexes:
        if text[i] == ALWAYS_STRONG_VOWEL:
            return i


def get_vowel_indexes(text: str) -> List[int]:
    vowel_indexes = []
    for i in range(len(text)):
        if text[i] in VOWELS:
            vowel_indexes.append(i)
    return vowel_indexes


def calculate_ending(text: str, vowel_indexes: List[int], strong_symbol_index: int) -> str:
    # For words without vowels
    syl_count = len(vowel_indexes)
    if syl_count == 0:
        return ''
    strong_vowel_index = None
    # If strong vowel is MARKED
    if strong_symbol_index is not None:
        strong_vowel_index = strong_symbol_index - 1
    # This condition is for estimation on raw word
    if strong_vowel_index is None:
        strong_vowel_index = estimate_strong_vowel(text, vowel_indexes)
    if strong_vowel_index is None:
        raise StrongVowelNotFound(text)
    return normalize_ending_vowels(text[strong_vowel_index:])


def prepare_word(text: str, strong_symbol_index: int) -> Tuple[int, str]:
    # syl_count, strong_vowel_index, last_vowel_index
    vowel_indexes = get_vowel_indexes(text)
    syl_count = len(vowel_indexes)
    ending = calculate_ending(text, vowel_indexes, strong_symbol_index)
    return syl_count, ending


def _exclude_non_vowels_ending(pre_words: List[str], _: Dict[str, Any]) -> bool:
    return not any(symbol in VOWELS for symbol in pre_words[-1])


class Processor(WordProcessor):

    def __init__(self, strong_symbol: str = '`'):
        self.strong_symbol = strong_symbol
        self.stemmer = None
        if StemmerClass:
            self.stemmer = StemmerClass()

    def get_syl_count(self, word: str) -> int:
        return len(get_vowel_indexes(word))

    def get_syl_count_and_ending(self, word: str) -> Optional[Tuple[int, str]]:
        try:
            strong_symbol_index = word.index(self.strong_symbol)
        except ValueError:
            strong_symbol_index = None
        try:
            return prepare_word(word.replace(self.strong_symbol, ''), strong_symbol_index)
        except StrongVowelNotFound:
            return

    def split_phrase_to_words(self, phrase: str) -> List[str]:
        return [reduce_word(word) for word in split_to_words(phrase, ALPHABET)]

    def guess_syl_count(self, text: str) -> Optional[int]:
        return sum(int(symbol in VOWELS) for symbol in text)

    def guess_word(self, text: str) -> Optional[Word]:
        last_vowel_index = None
        vowel_count = 0
        for index, symbol in enumerate(text):
            if symbol in VOWELS:
                vowel_count += 1
                last_vowel_index = index
        if vowel_count != 1:
            return None
        ending = ''.join(
            VOWELS_RHYME_MAP.get(symbol, symbol)
            for symbol in text[last_vowel_index:]
        )
        return Word(text, vowel_count, ending)

    def prewords_euristics(self) -> List[PrewordEuristic]:
        euristics = super().prewords_euristics()
        euristics.append(_exclude_non_vowels_ending)
        return euristics

    def get_tags(self, words: List[str]) -> List[str]:
        tags = set(words)
        if self.stemmer:
            for word in words:
                tags.add(self.stemmer.stem(word))
        return list(tags)
