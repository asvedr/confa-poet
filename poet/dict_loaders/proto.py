from abc import ABC, abstractmethod
from typing import Iterable, IO

from poet.base.word_base import Word


class ProtoLoader(ABC):
    def parse_file(self, handler: IO) -> Iterable[Word]: ...
