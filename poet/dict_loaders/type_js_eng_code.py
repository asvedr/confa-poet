import json
from typing import Dict, Iterable, IO, List, Optional

from poet.base.word_base import Word, WordBase
from poet.dict_loaders.proto import ProtoLoader
from poet.dict_loaders.errors import InvalidWordInDict


VOWELS = set('EYUIOA')
VALID_SYMBOLS = {chr(x) for x in range(ord('a'), ord('z') + 1)} | {"'"}
INVALID_SUFFIXES = {'abbrev'}


class Parser(ProtoLoader):

    def __init__(self):
        pass

    def _make_ending(self, sounds: List[str]) -> Optional[str]:
        # второй гласный звук с конца?
        vowels = []
        for i in reversed(range(len(sounds))):
            if sounds[i][0] in VOWELS:
                vowels.append(i)
                if len(vowels) == 2:
                    break
        if vowels:
            return ' '.join(sounds[vowels[-1]:])
        else:
            return None

    def _make_word(self, text: str, transcription: str) -> Optional[Word]:
        transcription = transcription.split('#')[0].strip()
        sounds = transcription.split(' ')
        vowels = [sound for sound in sounds if sound[0] in VOWELS]
        syl_count = len(vowels)
        ending = self._make_ending(sounds)
        if ending:
            return Word(text, syl_count, ending)

    def _valid_word(self, word: str) -> bool:
        return all(symbol in VALID_SYMBOLS for symbol in word)

    def _valid_transcription(self, transcription: str) -> bool:
        split = transcription.split('#')
        return len(split) == 1 or split[1] not in INVALID_SUFFIXES

    def parse_file(self, handler: IO) -> Iterable[Word]:
        js: Dict[str, str] = json.load(handler)
        for word, transcription in js.items():
            processed: Optional[Word] = None
            if self._valid_word(word) and self._valid_transcription(transcription):
                processed = self._make_word(word, transcription)
            if processed:
                yield processed
