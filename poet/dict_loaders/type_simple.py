from typing import Iterable, IO

from poet.base.word_base import Word, WordBase
from poet.dict_loaders.proto import ProtoLoader
from poet.dict_loaders.errors import InvalidWordInDict
from poet.langs.ru.processor import Processor


class Parser(ProtoLoader):

    def __init__(self):
        # self.base = WordBase(':memory:')
        self.strong_symbol = '`'
        self.processor = Processor(self.strong_symbol)

    def _parse_line(self, line: str) -> Iterable[Word]:
        line = line.strip()
        if not line:
            return
        data = line.split(',')
        for word_src in data:
            pair = self.processor.get_syl_count_and_ending(word_src)
            if not pair:
                raise InvalidWordInDict(word_src)
            syl_count, ending = pair
            yield Word(word_src.replace(self.strong_symbol, ''), syl_count, ending)

    def parse_file(self, handler: IO) -> Iterable[Word]:
        for line in handler:
            yield from self._parse_line(line)
