from typing import Callable, Dict, Iterable, List

from poet.dict_loaders.proto import ProtoLoader
from poet.dict_loaders import type_forms
from poet.dict_loaders import type_hagen
from poet.dict_loaders import type_simple
from poet.dict_loaders import type_js_eng_code
from poet.base.word_base import Word


class DictLoader(object):

    _modules = {
        'forms': type_forms,
        'hagen': type_hagen,
        'simple': type_simple,
        'js-eng-code': type_js_eng_code,
    }
    parsers: Dict[str, Callable[[], ProtoLoader]]

    def __init__(self):
        self.parsers = {key: module.Parser for key, module in self._modules.items()}

    @property
    def extensions(self) -> List[str]:
        return [f'.{key}.txt' for key in self.parsers.keys()]

    def load(self, path) -> Iterable[Word]:
        parser_cls = self.parsers.get(path.split('.')[-2])
        if not parser_cls:
            return
        parser = parser_cls()
        with open(path) as handler:
            yield from parser.parse_file(handler)
