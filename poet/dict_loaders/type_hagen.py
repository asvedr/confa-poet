from typing import Iterable, IO, Optional

from poet.dict_loaders.proto import ProtoLoader
from poet.base.word_base import Word
from poet.langs.ru.processor import Processor


class Parser(ProtoLoader):

    def __init__(self):
        symbols = ''.join(chr(i) for i in range(ord('а'), ord('я') + 1))
        symbols += 'ё'
        symbols += symbols.upper()
        self.ok_symbols = {c for c in symbols}
        self.strong_symbol = "'"
        self.ok_symbols.add(self.strong_symbol)
        # self.base = WordBase(':memory:')
        self.processor = Processor(self.strong_symbol)

    def parse_file(self, handler: IO) -> Iterable[Word]:
        for line in handler:
            word = self._parse_line(line)
            if word:
                yield word

    def _parse_line(self, line: str) -> Optional[Word]:
        line = line.strip()
        if not line:
            return
        word_src = line.split('|')[1].strip()
        if not all(c in self.ok_symbols for c in word_src):
            return
        pair = self.processor.get_syl_count_and_ending(word_src)
        if not pair:
            return
        syl_count, ending = pair
        return Word(word_src.replace(self.strong_symbol, ''), syl_count, ending)
