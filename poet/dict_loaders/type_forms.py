from typing import Iterable, IO

from poet.dict_loaders.proto import ProtoLoader
from poet.base.word_base import Word
from poet.langs.ru.processor import Processor


class Parser(ProtoLoader):

    def __init__(self):
        # self.base = WordBase(':memory:')
        self.strong_symbol = "'"
        self.processor = Processor(self.strong_symbol)

    def parse_file(self, handler: IO) -> Iterable[Word]:
        for line in handler:
            yield from self._parse_line(line)

    def _parse_line(self, line: str) -> Iterable[Word]:
        for word_src in line.split('#')[1].split(','):
            clean_text = word_src.replace('`', '').strip()
            pair = self.processor.get_syl_count_and_ending(clean_text)
            clean_text = clean_text.replace(self.strong_symbol, '')
            if pair:
                syl_count, ending = pair
                yield Word(clean_text, syl_count, ending)
