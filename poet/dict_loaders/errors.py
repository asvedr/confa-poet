class InvalidWordInDict(Exception):
    def __init__(self, word):
        self.word = word
