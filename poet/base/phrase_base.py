from typing import Any, Dict, List, Optional, Tuple, Union

from poet.base.proto import ProtoBase
from poet.base.word_base import Word, WordBase
from poet.base.phrase import Phrase, InvalidWord
from poet.base.rhyme_rules import RULES_FOR_RECALCULATION
from poet.langs.proto import WordProcessor
from poet.utils import euristic_exclude_decorator, check_euristics, EuristicError


def _exclude_list(exclude: List[Union[str, Phrase]]) -> str:
    slug_list = [
        '"%s"' % (phrase_or_slug.slug if isinstance(phrase_or_slug, Phrase) else phrase_or_slug)
        for phrase_or_slug in exclude
    ]
    return f'({",".join(slug_list)})'


def _syl_count_condition(table: str, field: str, value: Union[Tuple[int, int], int]) -> str:
    if isinstance(value, tuple):
        min_val, max_val = value
        return f'(`{table}`.`{field}` BETWEEN {min_val} AND {max_val})'
    else:
        return f'(`{table}`.`{field}` = {value})'


def _phrases_conditions(table: str, syl_count: int, exclude: List[Union[str, Phrase]]) -> List[str]:
    conditions = []
    if syl_count is not None:
        conditions.append(_syl_count_condition(table, 'syl_count', syl_count))
    if exclude:
        conditions.append(f'(`{table}`.`slug` NOT IN {_exclude_list(exclude)})')
    return conditions


def _rhymes_conditions(table: str, syl_count: int, exclude: List[Union[str, Phrase]]) -> List[str]:
    conditions = []
    if syl_count is not None:
        conditions.append(_syl_count_condition(table, 'rhyme_syl_count', syl_count))
    if exclude:
        conditions.append(f'(`{table}`.`rhyme_id` NOT IN {_exclude_list(exclude)})')
    return conditions


def _tags_conditions(tags: Optional[List[str]]) -> List[str]:
    if not tags:
        return []
    seq = ' OR '.join(f'`tag` LIKE "{tag}%"' for tag in tags)
    return [f'({seq})']
    # tags = ', '.join(f'"{tag}"' for tag in tags)
    # return [f'`tag` IN ({tags})']


class Phrases(object):
    text = 'TEXT NOT NULL'
    slug = 'TEXT PRIMARY KEY'
    end = 'TEXT NOT NULL'
    last_word = 'TEXT NOT NULL'
    syl_count = 'INTEGER NOT NULL'

    UNIQUE = ['slug', 'text']
    INDEXES = ['text', 'slug', 'end', 'last_word']

    _INSERT_COLUMNS = ['text', 'slug', 'end', 'last_word', 'syl_count']


class Rhymes(object):
    id = 'INTEGER PRIMARY KEY'
    phrase_id = 'TEXT NOT NULL'
    rhyme_id = 'TEXT NOT NULL'
    rhyme_syl_count = 'INTEGER NOT NULL'

    FOREIGN_KEYS = {
        'phrase_id': (Phrases, 'slug'),
        'rhyme_id': (Phrases, 'slug'),
    }
    INDEXES = ['phrase_id', 'rhyme_id', 'rhyme_syl_count']


class Tags(object):
    id = 'INTEGER PRIMARY KEY'
    tag = 'TEXT'
    phrase_id = 'TEXT NOT NULL'

    TABLE_NAME = 'tags_table'
    FOREIGN_KEYS = {
        'phrase_id': (Phrases, 'slug')
    }
    INDEXES = ['phrase_id', 'tag']

    _INSERT_COLUMNS = ['phrase_id', 'tag']


# class TagsPhrases(object):
#     id = 'INTEGER PRIMARY KEY'
#     tag_id = 'INTEGER NOT NULL'
#     phrase_id = 'INTEGER NOT NULL'
#
#     FOREIGN_KEYS = {
#         'tag_id': (Tags, 'id'),
#         'phrase_id': (Phrases, 'id')
#     }
#     INDEXES = ['tag_id', 'phrase_id']


def check_total_syl_count(syl_count: int, params: Dict[str, Any]) -> None:
    min_syl_count = params.get('min_syl_count')
    if min_syl_count is not None and syl_count < min_syl_count:
        raise EuristicError()
    max_syl_count = params.get('max_syl_count')
    if max_syl_count is not None and syl_count > max_syl_count:
        raise EuristicError()


class PhraseBase(ProtoBase):
    TABLES = [Phrases, Rhymes, Tags]

    def __init__(self, *args, **kwargs):
        self.word_base: WordBase = kwargs.pop('word_base', None)
        self.processor: WordProcessor = kwargs.pop('processor', None)
        super().__init__(*args, **kwargs)
        self._prewords_euristics = None
        self._before_ending_euristics = None
        self._after_ending_euristics = None
        if self.processor:
            self._prewords_euristics = self.processor.prewords_euristics()
            self._before_ending_euristics = self.processor.before_ending_euristics()
            self._after_ending_euristics = self.processor.after_ending_euristics()

    def get_size(self):
        return self.fetchone(f'SELECT COUNT(*) FROM `{self.table(Phrases)}`')[0]

    def _get_syl_count_sum(self, texts: List[str]) -> int:
        syl_count_sum, not_found = self.word_base.get_syl_count_sum(texts)
        for word in not_found:
            syl_count = self.processor.guess_syl_count(word)
            if syl_count is None:
                raise InvalidWord(word)
            syl_count_sum += syl_count
        return syl_count_sum

    def _get_word_with_ending(self, text: str) -> Word:
        word = self.word_base.get_word_by_text(text)
        if word:
            return word
        word = self.processor.guess_word(text)
        if word:
            return word
        raise InvalidWord(text)

    @euristic_exclude_decorator
    def prepare_phrase(self, text: str, euristic_params: Dict[str, Any]) -> Optional[Phrase]:
        assert self.word_base and self.processor
        text = text.strip()
        pre_words = self.processor.split_phrase_to_words(text)
        check_euristics(self._prewords_euristics, pre_words, euristic_params)
        total_syl_count = self._get_syl_count_sum(pre_words)
        # TODO add before_ending_euristics
        check_total_syl_count(total_syl_count, euristic_params)
        last_word = self._get_word_with_ending(pre_words[-1])
        if not last_word:
            raise InvalidWord(pre_words[-1])
        # TODO add after_ending_euristics
        tags = self.processor.get_tags(pre_words)
        return Phrase(
            ''.join(pre_words),
            text,
            last_word.end,
            last_word.text,
            total_syl_count,
            tags,
        )

    def add_phrases(self, phrases: List[Phrase]) -> None:
        t_phrases = []
        t_tags = []
        for phrase in phrases:
            t_phrases.append((phrase.text, phrase.slug, phrase.end, phrase.last_word, phrase.syl_count))
            t_tags.extend((phrase.slug, tag) for tag in phrase.tags)
        self.insertmany(Phrases, Phrases._INSERT_COLUMNS, t_phrases)
        self.insertmany(Tags, Tags._INSERT_COLUMNS, t_tags)

    def recalculate_rhymes(self, batch_size: int = 1000) -> None:
        # Remove old values
        self.recreate_table(Rhymes)
        iterator = self.iterate_over_table(Phrases, Phrase.table_field, Phrase.from_table, batch_size)
        for phrase in iterator:
            self._recalculate_rhymes_for_phrase(phrase)
        self.commit()

    def _recalculate_rhymes_for_phrase(self, phrase: Phrase) -> None:
        conditions = ' OR '.join(
            rule.condition(self.table(Phrases), phrase)
            for rule in RULES_FOR_RECALCULATION
        )
        query = (
            f'INSERT INTO `{self.table(Rhymes)}` '
            '(`phrase_id`, `rhyme_id`, `rhyme_syl_count`) '
            f'SELECT "{phrase.slug}", `slug`, `syl_count` FROM `{self.table(Phrases)}` '
            f'WHERE {conditions}'
        )
        self.execute(query)

    def _random_phrase_query(
            self,
            phrases_source: str,
            syl_count: int,
            rhyme_syl_count: int,
            exclude: List[Phrase],
            exclude_rhymes: List[Phrase],
            tags: Optional[List[str]],
    ) -> str:
        t_rhymes = self.table(Rhymes)
        t_tags = self.table(Tags)
        conditions = ' AND '.join(
            _phrases_conditions(phrases_source, syl_count, exclude) +
            _rhymes_conditions(t_rhymes, rhyme_syl_count, exclude_rhymes) +
            _tags_conditions(tags)
        )
        random_id_query = f'''
            SELECT DISTINCT `{phrases_source}`.`slug` FROM 
            `{phrases_source}` INNER JOIN `{t_rhymes}` 
            ON `{phrases_source}`.`slug` = `{t_rhymes}`.`phrase_id` 
        '''
        if tags:
            random_id_query += f'''
                INNER JOIN `{t_tags}`
                ON `{phrases_source}`.`slug` = `{t_tags}`.`phrase_id`
            '''
        if conditions:
            random_id_query += f'WHERE {conditions} '
        # Getting one random item from query
        random_id_query += 'ORDER BY RANDOM() LIMIT 1'
        return random_id_query

    def _select_phrase(self, slug: str) -> Phrase:
        t_phrases = self.table(Phrases)
        fields = Phrase.db_fields(t_phrases)
        fields = self.fetchone(f'SELECT {fields} FROM `{t_phrases}` WHERE `slug` = "{slug}"')
        return Phrase.from_table(*fields)

    def get_random_rhymed(
            self,
            syl_count: Optional[int] = None,
            rhyme_syl_count: Optional[int] = None,
            exclude: Optional[List[Phrase]] = None,
            exclude_rhymes: Optional[List[Phrase]] = None,
            tags: Optional[List[str]] = None,
    ) -> Optional[Phrase]:
        t_phrases = self.table(Phrases)
        query = self._random_phrase_query(
            t_phrases,
            syl_count,
            rhyme_syl_count,
            exclude,
            exclude_rhymes,
            tags,
        )
        phrase_id = self.fetchone(query)

        if not phrase_id:
            return

        return self._select_phrase(phrase_id[0])

    def get_random_unrhymed(
            self,
            syl_count: Optional[int] = None,
            exclude: Optional[List[Phrase]] = None,
            tags: Optional[List[str]] = None,
    ) -> Optional[Phrase]:
        """ In this case the base is checking that new phrase is not a rhyme to anythig in `exclude`
            Of course new phrase is not equal to anything in exclude 
        """
        t_phrases = self.table(Phrases)
        t_rhymes = self.table(Rhymes)
        t_tags = self.table(Tags)
        t_exclude_phrases = 'exclude_phrases'
        conditions = (
            _phrases_conditions(t_phrases, syl_count, exclude) +
            _tags_conditions(tags)
        )
        if exclude:
            subset_query = f'''
                SELECT `rhyme_id` as `id` FROM `{t_rhymes}` 
                WHERE `phrase_id` IN {_exclude_list(exclude)} 
            '''
            subset_query = f'WITH {t_exclude_phrases} as ({subset_query}) '
            main_select = f'''
                SELECT `{t_phrases}`.`slug` FROM 
                `{t_phrases}` LEFT OUTER JOIN `{t_exclude_phrases}` 
                ON `{t_phrases}`.`slug` = `{t_exclude_phrases}`.`id` 
            '''
            if tags:
                main_select += f'INNER JOIN `{t_tags}` ON `{t_tags}`.`phrase_id` = `{t_phrases}`.`slug` '
            conditions.append(f'(`{t_exclude_phrases}`.`id` IS NULL)')
            query = subset_query + main_select + ' WHERE ' + ' AND '.join(conditions)
        else:
            query = f'SELECT `slug` FROM {t_phrases} '
            if tags:
                query += f'INNER JOIN `{t_tags}` ON `{t_tags}`.`phrase_id` = `{t_phrases}`.`slug` '
            if conditions:
                query += ' WHERE ' + ' AND '.join(conditions)
        query += ' ORDER BY RANDOM() LIMIT 1'

        phrase_id = self.fetchone(query)
        if not phrase_id:
            return None

        return self._select_phrase(phrase_id[0])

    def get_rhyme(
            self,
            phrase: Phrase,
            syl_count: Optional[int] = None,
            new_rhyme_syl_count: Optional[int] = None,
            exclude: Optional[List[Phrase]] = None,
            exclude_new_rhymes: Optional[List[Phrase]] = None,
    ) -> Optional[Phrase]:
        if exclude:
            exclude = exclude[:]
            exclude.append(phrase)
        else:
            exclude = [phrase]
        t_phrases = self.table(Phrases)
        t_rhymes = self.table(Rhymes)
        t_filtered_phrases = 'filtered_phrases'

        phrases_subset_query = (
            'SELECT `rhyme_id` as `slug`, `rhyme_syl_count` as `syl_count` '
            f'FROM `{t_rhymes}` WHERE `phrase_id` = "{phrase.slug}"'
        )

        phrase_id_query = f'WITH {t_filtered_phrases} as ({phrases_subset_query}) '
        phrase_id_query += self._random_phrase_query(
            t_filtered_phrases,
            syl_count,
            new_rhyme_syl_count,
            exclude,
            exclude_new_rhymes,
            None,
        )

        phrase_id = self.fetchone(phrase_id_query)
        if not phrase_id:
            return

        return self._select_phrase(phrase_id[0])

    def change_end(self, text: str, new_end: str):
        query = f'UPDATE {self.table(Phrases)} SET `end` = ? WHERE `text` = ?'
        self.execute(query, (new_end, text))

    def change_last_word(self, text: str, new_word: str):
        query = f'UPDATE {self.table(Phrases)} SET `last_word` = ? WHERE `text` = ?'
        self.execute(query, (new_word, text))

    def get_phrase_by_text(self, text: str):
        t_phrases = self.table(Phrases)
        fields = Phrase.db_fields(t_phrases)
        fields = self.fetchone(f'SELECT {fields} FROM `{t_phrases}` WHERE `text` = ?', [text])
        if fields:
            return Phrase.from_table(*fields)

    def get_phrase_tags(self, phrase: Phrase) -> List[str]:
        return [
            values[0]
            for values in self.fetchall(f'SELECT `tag` FROM `{self.table(Tags)}` WHERE `phrase_id` = ?', [phrase.slug])
        ]
