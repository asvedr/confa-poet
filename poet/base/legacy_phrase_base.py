from typing import Iterable

from poet.base.proto import ProtoBase
from poet.base.phrase import Phrase


class Phrases(object):
    id = 'INTEGER PRIMARY KEY'
    text = 'TEXT NOT NULL'
    slug = 'TEXT NOT NULL'
    end = 'TEXT NOT NULL'
    last_word = 'TEXT NOT NULL'
    syl_count = 'INTEGER NOT NULL'


class LegacyPhraseBase(ProtoBase):
    TABLES = [Phrases]

    def read_all(self, batch_size: int = 1000) -> Iterable[Phrase]:
        yield from self.iterate_over_table(
            Phrases,
            ['slug', 'text', 'end', 'last_word', 'syl_count'],
            Phrase.from_table,
            batch_size
        )

    def get_size(self):
        return self.fetchone(f'SELECT COUNT(*) FROM `{self.table(Phrases)}`')[0]
