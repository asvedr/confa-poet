from poet.base.phrase import Phrase


class BaseRule(object):

    USE_IN_RECALCULATION = True

    @staticmethod
    def condition(table_source: str, phrase: Phrase):
        raise NotImplemented()


class EqEndSylCount(BaseRule):

    USE_IN_RECALCULATION = False

    @staticmethod
    def condition(table_source, phrase):
        return (
            f'(`{table_source}`.`syl_count` = {phrase.syl_count} AND '
            f'`{table_source}`.`end` = "{phrase.end}" AND '
            f'`{table_source}`.`last_word` <> "{phrase.last_word}")'
        )


class EqEnd(BaseRule):

    USE_IN_RECALCULATION = True

    @staticmethod
    def condition(table_source, phrase):
        return (
            f'(`{table_source}`.`end` = "{phrase.end}" AND '
            f'`{table_source}`.`last_word` <> "{phrase.last_word}")'
        )


RULES_FOR_CHECK = [EqEndSylCount, EqEnd]
RULES_FOR_RECALCULATION = [rule for rule in RULES_FOR_CHECK if rule.USE_IN_RECALCULATION]
