from typing import List, Optional, Tuple

from poet.base.proto import ProtoBase, ProtoRowObject


class StrongVowelNotFound(Exception):
    pass


class Word(ProtoRowObject):
    __slots__ = (
        'text',  # str
        'syl_count',  # int
        'end',  # str
    )


class WordBase(ProtoBase):

    class Words(object):
        text = 'TEXT PRIMARY KEY'
        syl_count = 'INTEGER NOT NULL'
        end = 'TEXT'
        INDEXES = ['text']

    TABLES = [Words]

    def get_size(self) -> int:
        """ get size of word base """
        return self.fetchone(f'SELECT COUNT(*) FROM `{self.table(self.Words)}`')[0]

    _text_syl_end = ['text', 'syl_count', 'end']

    def add_words(self, words: List[Word]) -> None:
        """ ... to base """
        data = [(word.text, word.syl_count, word.end) for word in words]
        self.insertmany(self.Words, self._text_syl_end, data)

    def add_word(self, word: Word) -> None:
        """ ... to base """
        self.insert(self.Words, self._text_syl_end, (word.text, word.syl_count, word.end))

    def get_word_by_text(self, text: str) -> Optional[Word]:
        """ get word from base by it's text """
        text = text.lower()
        value = self.fetchone(
            f'SELECT `syl_count`,`end` FROM `{self.table(self.Words)}` WHERE `text` = ?',
            [text],
        )
        if value:
            syl_count, end = value
            return Word(text, syl_count, end)

    def change_end(self, text: str, ending: str) -> None:
        query = f'UPDATE `{self.table(self.Words)}` SET `end` = ? WHERE `text` = ?'
        self.execute(query, [ending, text])

    def get_syl_count_sum(self, texts: List[str]) -> Tuple[int, List[str]]:
        req = ','.join(['?' for _ in texts])
        values = dict(self.fetchall(
            f'SELECT `text`,`syl_count` FROM `{self.table(self.Words)}` WHERE `text` IN ({req})',
            texts
        ))
        total = 0
        not_found = []
        for word in texts:
            syl_count = values.get(word)
            if syl_count is None:
                not_found.append(word)
            else:
                total += syl_count
        return total, not_found
