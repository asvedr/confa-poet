from functools import lru_cache
import sqlite3
import random
from functools import lru_cache
from typing import List, Callable, TypeVar, Any, Iterable


class ProtoRowObject(object):
    """ Class must override __slots__ """

    __slots__ = ()

    def pk(self):
        return getattr(self, self.__slots__[0])

    def _attrs_tuple(self):
        return tuple(getattr(self, attr) for attr in self.__slots__)

    def _attrs_dict(self):
        return {attr: getattr(self, attr) for attr in self.__slots__}

    def __init__(self, *args):
        assert len(args) == len(self.__slots__)
        for key, value in zip(self.__slots__, args):
            setattr(self, key, value)

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.pk() == other.pk()

    def __hash__(self):
        pk = self.pk()
        assert pk is not None
        return pk.__hash__

    def check_eq(self, other):
        return isinstance(other, self.__class__) and self._attrs_tuple() == other._attrs_tuple()

    def __str__(self):
        return f'{self.__class__.__name__}({self._attrs_dict()})'

    __repr__ = __str__


T = TypeVar('T')


class ProtoBase(object):

    TABLES = []

    def __init__(self, *args, **kwargs):
        self._connection = sqlite3.connect(*args, **kwargs)
        self._indexes = []
        self._create_table_query = {}
        for table in self.TABLES:
            name = self.table(table)
            fields = [
                f'`{attr}` {getattr(table, attr)}'
                for attr in dir(table)
                if attr.islower() and not attr.startswith('_')
            ]
            
            unique = getattr(table, 'UNIQUE', [])
            fields.extend(f'UNIQUE(`{field}`)' for field in unique)
            
            foreign_keys = getattr(table, 'FOREIGN_KEYS', {})
            fields.extend(
                f'FOREIGN KEY (`{field}`) REFERENCES `{self.table(ext_table)}`(`{ext_field}`)'
                for field, (ext_table, ext_field) in foreign_keys.items()
            )

            fields = ', '.join(fields)
            query = f'CREATE TABLE IF NOT EXISTS `{name}` ({fields})'
            self._create_table_query[name] = query
            self._connection.execute(query)
            self._indexes.extend([(table, field) for field in getattr(table, 'INDEXES', [])])

    def recreate_table(self, table: type):
        tablename = self.table(table)
        self._connection.execute(f'DROP TABLE `{tablename}`')
        self._connection.execute(self._create_table_query[tablename])

    @lru_cache(maxsize=8)
    def table(self, tclass: type) -> str:
        table = getattr(tclass, 'TABLE_NAME', tclass.__name__)
        return table.lower()

    def fetchall(self, *args):
        return self._connection.execute(*args).fetchall()

    def fetchone(self, *args):
        return self._connection.execute(*args).fetchone()

    def execute(self, *args):
        return self._connection.execute(*args)

    def insert(self, table: type, fields: List[str], values: List[Any]) -> None:
        self.insertmany(table, fields, [values])

    def insertmany(self, table: type, raw_fields: List[str], values: List[Any]) -> None:
        fields = ','.join([f'`{field}`' for field in raw_fields])
        template = ','.join(['?' for _ in raw_fields])
        table = self.table(table)
        query = f'INSERT OR IGNORE INTO `{table}` ({fields}) VALUES ({template})'
        self._connection.executemany(query, values)

    def commit(self) -> None:
        self._connection.commit()

    def set_indexes(self) -> None:
        for table, field in self._indexes:
            table_name = self.table(table)
            index = self._make_index_name(table_name, field)
            query = f'CREATE INDEX IF NOT EXISTS {index} ON `{table_name}` (`{field}`)'
            self._connection.execute(query)
        self.commit()

    def drop_indexes(self) -> None:
        for table, field in self._indexes:
            table_name = table.__name__.lower()
            index = self._make_index_name(table_name, field)
            query = f'DROP INDEX IF EXISTS {index}'
            self._connection.execute(query)
        self.commit()

    def _make_index_name(self, table_name: str, field_name: str) -> str:
        return f'{table_name}_{field_name}_index'

    def iterate_over_table(
            self,
            table: type,
            columns: List[str],
            constructor: Callable[[List[Any]], T],
            batch_size: int,
    ) -> Iterable[T]:
        columns = ','.join(f'`{column}`' for column in columns)
        template = f'SELECT {columns} FROM `{self.table(table)}` LIMIT {batch_size} OFFSET %s'
        offset = 0
        while True:
            query = template % offset
            rows = self._connection.execute(query).fetchall()
            if not rows:
                break
            for row in rows:
                yield constructor(*row)
            offset += len(rows)
