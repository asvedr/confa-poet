from functools import lru_cache

from poet.base.proto import ProtoRowObject


class InvalidWord(Exception):
    def __init__(self, word):
        self.word = word


class Phrase(ProtoRowObject):
    table_field = (
        'slug',  # str
        'text',  # str
        'end',  # str
        'last_word',  # str
        'syl_count',  # int
    )
    __slots__ = tuple(
        list(table_field) +
        ['tags']  # List[str]
    )

    @classmethod
    @lru_cache(maxsize=16)
    def db_fields(cls, table):
        return ','.join(f'`{table}`.`{field}`' for field in cls.__slots__ if field != 'tags')

    @classmethod
    def from_table(cls, *args):
        return cls(*args, [])

    def check_eq(self, other):
        return isinstance(other, self.__class__) and self._fields_eq_check(other)

    def _fields_eq_check(self, other):
        self_attrs = self._attrs_dict()
        del self_attrs['tags']
        other_attrs = other._attrs_dict()
        del other_attrs['tags']
        return self_attrs == other_attrs
