import logging
import random
from typing import Dict, List, Tuple, Optional

from poet.base.phrase_base import PhraseBase, Phrase
from poet.verse_config import VerseConfig, ProtoRow
from poet.utils import retry_func


log = logging.getLogger(__file__)


class GenerationFailed(Exception):
    def __init__(self, schema_name, row_index):
        self.schema_name = schema_name
        self.row_index = row_index

    def __str__(self):
        return (
            'GenerationFailed'
            f'(schema_name={self.schema_name}, row_index={self.row_index})'
        )


def _upper_first_symbol(text):
    symbol = text[0].upper()
    return symbol + text[1:]


class Poet(object):

    @classmethod
    def load(cls, phrase_base_path: str, verse_config: Dict[str, Dict], **kwargs):
        phrase_base = PhraseBase(phrase_base_path)
        assert phrase_base.get_size() > 0
        schemas = [
            VerseConfig(key, value)
            for key, value in verse_config.items()
        ]
        return cls(phrase_base, schemas)

    def __init__(
            self,
            phrase_base: PhraseBase,
            verse_schemas: List[VerseConfig],
            retry_count=10,
        ):
        self.phrase_base = phrase_base
        self.schemas = verse_schemas
        self.retry_count = retry_count

    def _get_rhyme_syl_count(
            self,
            row: ProtoRow,
            schemas: List[ProtoRow],
            row_syl_counts: List[int]
        ):
        if row.next_rhyme is None:
            return
        schema = schemas[row.next_rhyme]
        return schema.get_size(row_syl_counts, as_rhyme=True)

    def _gen_rhymed_row(
            self,
            row_schema: ProtoRow,
            all_row_schemas: List[ProtoRow],
            row_syl_counts: List[int],
            already_created_rows: List[Phrase],
            tags: Optional[List[str]],
    ) -> Tuple[Phrase, bool]:
        rhyme_syl_count = self._get_rhyme_syl_count(
            row_schema,
            all_row_schemas,
            row_syl_counts,
        )
        current_syl_count = row_syl_counts[-1]
        exclude_rhymes = []
        if row_schema.next_rhyme is not None:
            exclude_rhymes = already_created_rows
        tag_used = True
        if row_schema.rhyme_of is None:
            row = self.phrase_base.get_random_rhymed(
                syl_count=current_syl_count,
                rhyme_syl_count=rhyme_syl_count,
                exclude=already_created_rows,
                exclude_rhymes=exclude_rhymes,
                tags=tags,
            )
            if row is None and row_schema.tag_can_be_missed:
                tag_used = False
                row = self.phrase_base.get_random_rhymed(
                    syl_count=current_syl_count,
                    rhyme_syl_count=rhyme_syl_count,
                    exclude=already_created_rows,
                    exclude_rhymes=exclude_rhymes,
                    tags=[],
                )
        else:
            row = self.phrase_base.get_rhyme(
                already_created_rows[row_schema.rhyme_of],
                syl_count=current_syl_count,
                new_rhyme_syl_count=rhyme_syl_count,
                exclude=already_created_rows,
                exclude_new_rhymes=exclude_rhymes,
            )
        return row, tag_used

    def gen_rows(self, schema: VerseConfig, tags: Optional[List[str]]) -> List[Phrase]:
        rows = []
        row_syl_counts = []
        phrase_base = self.phrase_base
        for index, row_schema in enumerate(schema.rows):
            syl_count = row_schema.get_size(row_syl_counts, as_rhyme=False)
            row_syl_counts.append(syl_count)
            if row_schema.has_rhyme:
                row, tag_used = self._gen_rhymed_row(
                    row_schema, schema.rows, row_syl_counts, rows, tags
                )
            else:
                row = phrase_base.get_random_unrhymed(
                    syl_count=syl_count, exclude=rows, tags=tags
                )
                tag_used = True
            if row is None:
                raise GenerationFailed(schema.name, index)
            if tags and tag_used:
                tags = None
            rows.append(row)
        return rows

    def gen_verse(
            self,
            schema: VerseConfig,
            upper_first_symbol: bool = False,
            tags: Optional[List[str]] = None,
    ) -> str:

        @retry_func(exceptions=(GenerationFailed), count=self.retry_count)
        def wrapper():
            return self.gen_rows(schema, tags)

        rows = wrapper()
        if upper_first_symbol:
            return '\n'.join(_upper_first_symbol(row.text) for row in rows)
        return '\n'.join(row.text for row in rows)

    def gen_all_schemas(
            self,
            upper_first_symbol: bool = False,
            tags: Optional[List[str]] = None
    ) -> List[Tuple[str, str]]:
        verses = []
        for verse_schema in self.schemas:
            try:
                verse = self.gen_verse(verse_schema, upper_first_symbol, tags)
                verses.append((verse_schema.name, verse))
            except GenerationFailed:
                pass
        return verses

    def gen_random_verse(self, upper_first_symbol: bool = False, tags: Optional[List[str]] = None) -> str:
        # verses = self.gen_all_schemas(upper_first_symbol)
        schemas = self.schemas[:]
        random.shuffle(schemas)
        for schema in schemas:
            try:
                return self.gen_verse(schema, upper_first_symbol, tags)
            except GenerationFailed:
                pass
